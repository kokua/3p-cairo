#!/bin/bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(dirname "$0")"

PROJECT=cairo
LICENSE=README
VERSION="1.14.12"
SOURCE_DIR="$PROJECT"


if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

stage="$(pwd)"
GLIB_INCLUDE="${stage}"/packages/include/glib
PIXMAN_INCLUDE="${stage}"/packages/include/pixman-1
LIBPNG_INCLUDE="${stage}"/packages/include/libpng16
FREETYPE_INCLUDE="${stage}"/packages/include/freetype2/freetype
FONTCONFIG_INCLUDE="${stage}"/packages/include/fontconfig
 
[ -f "$FONTCONFIG_INCLUDE"/fcfreetype.h ] || fail "You haven't installed the fontconfig package yet."
[ -f "$FREETYPE_INCLUDE"/freetype.h ] || fail "You haven't installed the freetype package yet."
[ -f "$LIBPNG_INCLUDE"/png.h ] || fail "You haven't installed the libpng package yet."
[ -f "$GLIB_INCLUDE"/glib.h ] || fail "You haven't installed the glib package yet."
[ -f "$PIXMAN_INCLUDE"/pixman.h ] || fail "You haven't installed the pixman package package yet."

#CAIRO_SOURCE_DIR="$PROJECT"
#VERSION="$(sed -n 's/^ *VERSION=\([0-9.]*\)$/\1/p' "../$CAIRO_SOURCE_DIR/configure")"

# load autbuild provided shell functions and variables
source_environment_tempfile="${stage}/source_environment.sh"
"$autobuild" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

build=${AUTOBUILD_BUILD_ID:=0}
echo "${VERSION}.${build}" > "${stage}/VERSION.txt"
case "$AUTOBUILD_PLATFORM" in
    "linux")
        # Prefer gcc-4.9 if available.
        if [[ -x /usr/bin/gcc-4.9 && -x /usr/bin/g++-4.9 ]]; then
            export CC=/usr/bin/gcc-4.9
            export CXX=/usr/bin/g++-4.9
        fi

        # Default target to 32-bit
        opts="${TARGET_OPTS:--m32}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        pushd "$TOP/$SOURCE_DIR"
           export PKG_CONFIG_PATH="$stage/packages/lib/pkgconfig"
           export PATH="$stage/packages/include":$PATH
           CFLAGS="-I$stage/packages/include \
                   -I$GLIB_INCLUDE \
                   -I$PIXMAN_INCLUDE \
                   -I$LIBPNG_INCLUDE \
                   -I$FONTCONFIG_INCLUDE \
                   -I$FREETYPE_INCLUDE $opts -O3 -fPIC -DPIC"\
            ./configure --prefix="$stage" \
			--with-glib-includes=$stage/packages/include/
            make
            make install
		    mkdir -p "$stage/include/cairo"
		    cp -a src/*.h "$stage/include/cairo"
        popd

        mv lib release
        mkdir -p lib
        mv release lib

    ;;
    "linux64")
        # Prefer gcc-4.6 if available.
        if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
            export CC=/usr/bin/gcc-4.6
            export CXX=/usr/bin/g++-4.6
        fi

        # Default target to 64-bit
        opts="${TARGET_OPTS:--m64}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        pushd "$TOP/$SOURCE_DIR"
           export PKG_CONFIG_PATH="$stage/packages/lib/release/pkgconfig"
           export PATH="$stage/packages/include":$PATH
           CFLAGS="-I$stage/packages/include \
                   -I$GLIB_INCLUDE \
                   -I$PIXMAN_INCLUDE \
                   -I$LIBPNG_INCLUDE \
                   -I$FONTCONFIG_INCLUDE \
                   -I$FREETYPE_INCLUDE $opts -O3 -fPIC -DPIC"\
            ./configure --prefix="$stage" \
			--with-glib-includes=$stage/packages/include/
            make
            make install
		    mkdir -p "$stage/include/cairo"
		    cp -a src/*.h "$stage/include/cairo"
        popd

        mv lib release
        mkdir -p lib
        mv release lib
    ;;

esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$SOURCE_DIR/$LICENSE" "$stage/LICENSES/$PROJECT.txt"



